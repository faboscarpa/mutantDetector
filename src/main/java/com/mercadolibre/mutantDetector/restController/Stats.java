package com.mercadolibre.mutantDetector.restController;

import com.mercadolibre.mutantDetector.model.StatsDTO;
import com.mercadolibre.mutantDetector.services.StatsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;

/**
 * RestControll for stats
 */
@RestController
@RequestMapping(value = "/stats")
@Log
@RequiredArgsConstructor
public class Stats {

    private final StatsService statsService;

    /**
     * returns the total of humans and mutants analyzed and their ratio.
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public StatsDTO getStats() {
        log.log(Level.INFO, () -> "/POST mutant ");
        return statsService.getStats();
    }
}
