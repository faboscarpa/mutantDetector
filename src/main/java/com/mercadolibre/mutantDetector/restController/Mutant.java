package com.mercadolibre.mutantDetector.restController;


import com.mercadolibre.mutantDetector.model.DNAAnalyzeDTO;
import com.mercadolibre.mutantDetector.services.DNAService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Level;


/**
 * RestControll for mutant
 */
@RestController
@RequestMapping(value = "/mutant")
@Log
@RequiredArgsConstructor
public class Mutant {

    private final DNAService dnaService;

    /**
     * Analyze a ADN and return:
     * 200 if is mutant
     * 400 bad request if it is an invalid DNA
     * 403 forbidden if is human
     * 500 server error
     *
     * @param request valid DNA
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void analyzeDNA(@RequestBody DNAAnalyzeDTO request) {
        log.log(Level.INFO, () -> "/POST mutant ");

        dnaService.isMutant(request.getDna());

    }

}
