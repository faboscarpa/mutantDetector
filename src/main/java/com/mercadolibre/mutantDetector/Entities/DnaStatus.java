package com.mercadolibre.mutantDetector.Entities;

public enum DnaStatus {
    HUMAN, MUTANT, ERROR, INVALID

}
