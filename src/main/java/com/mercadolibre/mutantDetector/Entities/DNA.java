package com.mercadolibre.mutantDetector.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "dnas")
@AllArgsConstructor
@Data
public class DNA {

    public DNA() {
        super();
    }

    private List<String> dna;

    @Indexed
    private DnaStatus status;


}
