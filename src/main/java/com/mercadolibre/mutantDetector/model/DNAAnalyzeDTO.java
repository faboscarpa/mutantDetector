package com.mercadolibre.mutantDetector.model;

import lombok.Data;

import java.util.List;

@Data
public class DNAAnalyzeDTO {

    private List<String> dna;

}
