package com.mercadolibre.mutantDetector.model;

import lombok.Data;

@Data
public class StatsDTO {

    private Long countMutantDna;
    private Long countHumanDna;
    private Float ratio;

}
