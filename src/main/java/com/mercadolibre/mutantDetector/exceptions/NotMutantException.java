package com.mercadolibre.mutantDetector.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Human DNA
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class NotMutantException extends RuntimeException {

    public NotMutantException(String message) {
        super(message);
    }

}
