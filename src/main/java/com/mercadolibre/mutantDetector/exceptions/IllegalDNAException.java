package com.mercadolibre.mutantDetector.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Invalid DNA.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class IllegalDNAException extends RuntimeException {

    public IllegalDNAException(String message) {
        super(message);
    }

}
