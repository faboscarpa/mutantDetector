package com.mercadolibre.mutantDetector.services;

import com.mercadolibre.mutantDetector.model.StatsDTO;

public interface StatsService {
    /**
     * obtain the human and mutant totals. Then calculate the proportion
     */
    StatsDTO getStats();
}
