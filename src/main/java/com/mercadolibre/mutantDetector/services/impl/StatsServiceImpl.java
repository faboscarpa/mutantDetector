package com.mercadolibre.mutantDetector.services.impl;

import com.mercadolibre.mutantDetector.Entities.DnaStatus;
import com.mercadolibre.mutantDetector.model.StatsDTO;
import com.mercadolibre.mutantDetector.repositories.DNARepository;
import com.mercadolibre.mutantDetector.services.StatsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Service
@Log
@RequiredArgsConstructor
public class StatsServiceImpl implements StatsService {

    private final DNARepository dnaRepository;

    @Override
    public StatsDTO getStats() {
        long countMutant = dnaRepository.countDNAByStatus(DnaStatus.MUTANT);
        long countHuman = dnaRepository.countDNAByStatus(DnaStatus.HUMAN);
        StatsDTO stats = new StatsDTO();
        stats.setCountHumanDna(countHuman);
        stats.setCountMutantDna(countMutant);
        if (countHuman > 0)
            stats.setRatio((float) countMutant / countHuman);
        else if (countMutant > 0)
            stats.setRatio(1f);
        return stats;
    }
}
