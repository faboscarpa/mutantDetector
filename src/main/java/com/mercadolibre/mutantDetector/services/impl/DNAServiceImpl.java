package com.mercadolibre.mutantDetector.services.impl;

import com.mercadolibre.mutantDetector.Entities.DNA;
import com.mercadolibre.mutantDetector.Entities.DnaStatus;
import com.mercadolibre.mutantDetector.exceptions.IllegalDNAException;
import com.mercadolibre.mutantDetector.exceptions.NotMutantException;
import com.mercadolibre.mutantDetector.repositories.DNARepository;
import com.mercadolibre.mutantDetector.services.DNAService;
import com.mercadolibre.mutantDetector.utils.DNAAnalyzerUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.logging.Level;
import java.util.stream.IntStream;


@Service
@Log
@RequiredArgsConstructor
public class DNAServiceImpl implements DNAService {

    private static final String VALID_NITROGEN_BASE_REGEX = "([atgc]*)";

    private final DNAAnalyzerUtils dnaAnalyzerUtils;

    private final DNARepository dnaRepository;

    @Override
    public void isMutant(List<String> dna) {
        DNA dnaEntitie = new DNA();
        dnaEntitie.setDna(dna);
        try {
            validateDNA(dna);
            char[][] dnaMatriz = convertDNAToMatriz(dna);
            dnaAnalyzer(dnaMatriz);
            dnaEntitie.setStatus(DnaStatus.MUTANT);
        } catch (IllegalDNAException e) {
            dnaEntitie.setStatus(DnaStatus.INVALID);
            throw e;
        } catch (NotMutantException enm) {
            dnaEntitie.setStatus(DnaStatus.HUMAN);
            throw enm;
        } catch (Exception ex) {
            dnaEntitie.setStatus(DnaStatus.ERROR);
            throw ex;
        } finally {
            dnaRepository.save(dnaEntitie);
        }

    }

    private void dnaAnalyzer(char[][] dnaMatriz) throws NotMutantException {
        Boolean isMutant = false;
        int mutantSequence = 0;
        forLine:
        for (int line = 0; line < dnaMatriz.length; line++) {
            for (int col = 0; col < dnaMatriz[line].length; col++) {

                if (dnaAnalyzerUtils.analyzeChainRight(dnaMatriz, line, col)) {
                    mutantSequence++;
                    if (mutantSequence == 2) break forLine;
                }

                if (dnaAnalyzerUtils.analyzeChainDown(dnaMatriz, line, col)) {
                    mutantSequence++;
                    if (mutantSequence == 2) break forLine;
                }

                if (dnaAnalyzerUtils.analyzeChainDiagonalRight(dnaMatriz, line, col)) {
                    mutantSequence++;
                    if (mutantSequence == 2) break forLine;
                }

                if (dnaAnalyzerUtils.analyzeChainDiagonalLeft(dnaMatriz, line, col)) {
                    mutantSequence++;
                    if (mutantSequence == 2) break forLine;
                }
            }
        }

        if (mutantSequence < 2)
            throw new NotMutantException("It's not a mutant :(");


    }


    private void validateDNA(List<String> dna) throws IllegalDNAException {
        if (CollectionUtils.isEmpty(dna)) {
            log.log(Level.SEVERE, () -> "The DNA chain can't be empty or null");
            throw new IllegalDNAException("The DNA chain can't be empty or null");
        }

        dna.forEach(dnaChain ->
        {
            if (!dnaChain.toLowerCase().matches(VALID_NITROGEN_BASE_REGEX))
                throw new IllegalDNAException("Invalid DNA chain");
        });
    }

    private char[][] convertDNAToMatriz(List<String> dna) {

        char[][] dnaMatriz = new char[dna.size()][];

        IntStream.range(0, dna.size()).forEach(index -> dnaMatriz[index] = dna.get(index).toCharArray());

        return dnaMatriz;
    }
}
