package com.mercadolibre.mutantDetector.services;

import com.mercadolibre.mutantDetector.exceptions.IllegalDNAException;
import com.mercadolibre.mutantDetector.exceptions.NotMutantException;

import java.util.List;

/**
 * Service for DNA tasks.
 */
public interface DNAService {
    /**
     * Analyze a DNA and answer if it is a mutant
     *
     * @param dna List <String> with valid DNA
     *            return void if the DNA is mutant
     * @throws IllegalDNAException invalid DNA
     * @throws NotMutantException  human DNA
     */
    void isMutant(List<String> dna);
}
