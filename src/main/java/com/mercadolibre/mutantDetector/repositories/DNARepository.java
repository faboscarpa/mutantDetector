package com.mercadolibre.mutantDetector.repositories;

import com.mercadolibre.mutantDetector.Entities.DNA;
import com.mercadolibre.mutantDetector.Entities.DnaStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DNARepository extends MongoRepository<DNA, Long> {

    Long countDNAByStatus(DnaStatus status);
}
