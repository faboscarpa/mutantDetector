package com.mercadolibre.mutantDetector.utils;

import org.springframework.stereotype.Component;

/**
 * Logic of DNA analysis
 */
@Component
public class DNAAnalyzerUtils {

    public boolean analyzeChainRight(char[][] dnaMatriz, int line, int col) {
        return analyzeChain(0, 1, dnaMatriz, line, col);
    }

    public boolean analyzeChainDown(char[][] dnaMatriz, int line, int col) {
        return analyzeChain(1, 0, dnaMatriz, line, col);
    }

    public boolean analyzeChainDiagonalRight(char[][] dnaMatriz, int line, int col) {
        return analyzeChain(1, 1, dnaMatriz, line, col);
    }

    public boolean analyzeChainDiagonalLeft(char[][] dnaMatriz, int line, int col) {
        return analyzeChain(1, -1, dnaMatriz, line, col);
    }

    /**
     * Given a position of the matrix compare with the next character according to the line and column increments.
     * If it is equal, it continues comparing with the next one, always increasing towards the same direction.
     * If it is different, it invokes itself with this new position
     */
    private boolean analyzeChain(int lineIncrement, int colIncrement, char[][] dnaMatriz, int line, int col) {
        int lineMax = line + (lineIncrement * 4);
        int colMax = col + (colIncrement * 4);
        if (lineMax < 0 || lineMax > dnaMatriz.length)
            return false;
        if (colMax < 0 || colMax > dnaMatriz[line].length)
            return false;
        Boolean isMutantChain = false;
        Boolean isValid = true;
        int count = 1;
        while (isValid && !isMutantChain) {

            if (dnaMatriz[line][col] == dnaMatriz[line + lineIncrement][col + colIncrement]) {
                if (++count == 4)
                    isMutantChain = true;
                line = line + lineIncrement;
                col = col + colIncrement;
            } else {
                analyzeChain(lineIncrement, colIncrement, dnaMatriz, line + lineIncrement, col + colIncrement);
                isValid = false;
            }

        }
        return isMutantChain;
    }


}
