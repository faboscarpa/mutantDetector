package com.mercadolibre.mutantDetector.restController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.mutantDetector.Entities.DNA;
import com.mercadolibre.mutantDetector.Entities.DnaStatus;
import com.mercadolibre.mutantDetector.model.StatsDTO;
import com.mercadolibre.mutantDetector.repositories.DNARepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureDataMongo
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StatsTest {

    private static final String URI_TEST = "http://localhost:8080/stats";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DNARepository repository;


    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {
        jsonMapper = new ObjectMapper();
    }


    @Test
    public void stats_emptyDB() throws Exception {


        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(URI_TEST).contentType(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
        System.out.println(result.andReturn().getResponse().getContentAsString());
        StatsDTO stats = jsonMapper.readValue(result.andReturn().getResponse().getContentAsString(), StatsDTO.class);

        Assert.assertEquals(new Long(0), stats.getCountHumanDna());


    }

    @Test
    public void statswithData() throws Exception {

        addDNAToMongo(100, 40);
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(URI_TEST).contentType(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
        System.out.println(result.andReturn().getResponse().getContentAsString());
        StatsDTO stats = jsonMapper.readValue(result.andReturn().getResponse().getContentAsString(), StatsDTO.class);

        Assert.assertEquals(new Long(100), stats.getCountHumanDna());
        Assert.assertEquals(new Long(40), stats.getCountMutantDna());
        Assert.assertEquals(new Float(0.4), stats.getRatio());


    }

    @Test
    public void statswithOnlyMutant() throws Exception {

        addDNAToMongo(0, 10);
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(URI_TEST).contentType(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
        System.out.println(result.andReturn().getResponse().getContentAsString());
        StatsDTO stats = jsonMapper.readValue(result.andReturn().getResponse().getContentAsString(), StatsDTO.class);

        Assert.assertEquals(new Long(0), stats.getCountHumanDna());
        Assert.assertEquals(new Long(10), stats.getCountMutantDna());
        Assert.assertEquals(new Float(1), stats.getRatio());


    }

    @Test
    public void statswithOnlyHuman() throws Exception {

        addDNAToMongo(10, 0);
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(URI_TEST).contentType(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
        System.out.println(result.andReturn().getResponse().getContentAsString());
        StatsDTO stats = jsonMapper.readValue(result.andReturn().getResponse().getContentAsString(), StatsDTO.class);

        Assert.assertEquals(new Long(10), stats.getCountHumanDna());
        Assert.assertEquals(new Long(0), stats.getCountMutantDna());
        Assert.assertEquals(new Float(0), stats.getRatio());


    }

    private void addDNAToMongo(int human, int mutant) {

        for (int i = 0; i < human; i++) {
            repository.insert(new DNA(new ArrayList<>(), DnaStatus.HUMAN));
        }
        for (int i = 0; i < mutant; i++) {
            repository.insert(new DNA(new ArrayList<>(), DnaStatus.MUTANT));
        }
    }

}