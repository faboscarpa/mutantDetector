package com.mercadolibre.mutantDetector.restController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.mutantDetector.Entities.DnaStatus;
import com.mercadolibre.mutantDetector.model.DNAAnalyzeDTO;
import com.mercadolibre.mutantDetector.repositories.DNARepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureDataMongo
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MutantTest {

    private static final String URI_TEST = "http://localhost:8080/mutant";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DNARepository repository;

    private ObjectMapper jsonMapper;


    @Before
    public void setUp() {
        jsonMapper = new ObjectMapper();
    }


    @Test
    public void isMutant_validMutant() throws Exception {
        DNAAnalyzeDTO dna = getMutantDna();
        String body = jsonMapper.writeValueAsString(dna);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URI_TEST).contentType(MediaType.APPLICATION_JSON).content(body));
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(DnaStatus.MUTANT, repository.findAll().get(0).getStatus());


    }

    @Test
    public void isMutant_null() throws Exception {
        String body = jsonMapper.writeValueAsString(null);
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URI_TEST).contentType(MediaType.APPLICATION_JSON).content(body));
        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        result.andExpect(MockMvcResultMatchers.handler().handlerType(Mutant.class));

    }

    @Test
    public void isMutant_emptyDna() throws Exception {

        DNAAnalyzeDTO dna = getEmptyDna();
        String body = jsonMapper.writeValueAsString(dna);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URI_TEST).contentType(MediaType.APPLICATION_JSON).content(body));
        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        result.andExpect(MockMvcResultMatchers.handler().handlerType(Mutant.class));

    }

    @Test
    public void isMutant_invalidDna() throws Exception {
        DNAAnalyzeDTO dna = getInvalidDna();

        String body = jsonMapper.writeValueAsString(dna);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URI_TEST).contentType(MediaType.APPLICATION_JSON).content(body));
        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        result.andExpect(MockMvcResultMatchers.handler().handlerType(Mutant.class));
    }

    @Test
    public void isMutant_humanDna() throws Exception {
        DNAAnalyzeDTO dna = getHumanDna();
        String body = jsonMapper.writeValueAsString(dna);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(URI_TEST).contentType(MediaType.APPLICATION_JSON).content(body));
        result.andExpect(MockMvcResultMatchers.status().isForbidden());
        result.andExpect(MockMvcResultMatchers.handler().handlerType(Mutant.class));

        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(DnaStatus.HUMAN, repository.findAll().get(0).getStatus());
    }

    private List<DNAAnalyzeDTO> createsDNA(int cant) {
        List<DNAAnalyzeDTO> dnas = new ArrayList<>();
        for (int i = 0; i < cant; i++) {
            dnas.add(getMutantDna());
        }
        return dnas;
    }


    private DNAAnalyzeDTO getHumanDna() {
        DNAAnalyzeDTO dnaDTO = new DNAAnalyzeDTO();
        List<String> dnas = new ArrayList<>();
        dnas.add("TTGCAA");
        dnas.add("CAGTGC");
        dnas.add("TTATGT");
        dnas.add("AGAAGG");
        dnas.add("CTCCTA");
        dnas.add("TCACTG");
        dnaDTO.setDna(dnas);
        return dnaDTO;
    }

    private DNAAnalyzeDTO getInvalidDna() {
        DNAAnalyzeDTO dnaDTO = new DNAAnalyzeDTO();
        List<String> dnas = new ArrayList<>();
        dnas.add("ATGCGA");
        dnas.add("CAGTGC");
        dnas.add("TTXTGT");
        dnas.add("AGAAGG");
        dnas.add("CCCCTA");
        dnas.add("TCACTG");
        dnaDTO.setDna(dnas);
        return dnaDTO;
    }

    private DNAAnalyzeDTO getEmptyDna() {
        DNAAnalyzeDTO dnaDTO = new DNAAnalyzeDTO();
        List<String> dnas = new ArrayList<>();
        return dnaDTO;
    }

    private DNAAnalyzeDTO getMutantDna() {
        DNAAnalyzeDTO dnaDTO = new DNAAnalyzeDTO();
        List<String> dnas = new ArrayList<>();
        dnas.add("ATGCGA");
        dnas.add("CAGTGC");
        dnas.add("TTATGT");
        dnas.add("AGAAGG");
        dnas.add("CCCCTA");
        dnas.add("TCACTG");
        dnaDTO.setDna(dnas);
        return dnaDTO;
    }

}
