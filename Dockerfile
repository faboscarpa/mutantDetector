FROM maven:3.5.3-jdk-8
COPY ./src /src
COPY ./pom.xml .
COPY ./.git .git/
RUN mvn clean package -Dmaven.artifact.threads=8

FROM openjdk:8-jre
ENV TZ="America/Argentina/Buenos_Aires"
RUN mkdir /app
WORKDIR /app
COPY --from=0 target/mutantDetector*.jar  .

EXPOSE 8080
CMD java -jar mutantDetector*.jar