# Mutan Detector

#### Solución planteada
Luego de varios casos de prueba, encontré que la solución más eficiente es la de convertir la secuencia de ADN en una matriz de caracteres y recorrerla de
forma secuencial hacia las cuatro direcciones posibles (derecha, abajo, diag izq y diag derecha).
Previo a comparar me aseguro que la secuencia entre dentro del largo de la línea en cuestión para optimizar las comparaciones
, tambien intente almacenar las posiciones y direcciones ya analizadas, pero esto hace más ineficiente el servicio aumentando
los tiempos de respuesta, memoria utilizada y consumo procesador.

## Ejecución

Para ejecutar el código en la raíz del proyecto ejecutar:
* docker-compose build
* docker-compose up

### Prerequisitos

* [Docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/)
* [project lombock](<https://projectlombok.org>) Para poder abrir el codigo desde algun ide sin dar error de compilación.

## Tests

Para ejecutar los test ejecutar en la raíz, mvn test.
<br/><br/>Para este ejercicio se optó por realizar test de componentes de ambos services (/mutant y /stats) utilizando MockMvc y un mongo en memoria.
<br/>

Además se adjunta un test de JMeter para evaluar el comportamiento con carga, tener en cuenta que el servicio está deployado en una máquina t2.micro de AWS sin autoscaling 💣.

## Deploy

El proyecto cuenta con CI de gitlab el cual buildea el docker y lo pushea a un repositorio de aws siempre que se pushee a master. 

## Built With

* [Maven](https://maven.apache.org/)
* [Spring Boot 2.0.3](https://spring.io/projects/spring-boot)
* [Project Lombok](https://projectlombok.org>)
* [Swagger](https://swagger.io/)
* [Mongo](https://www.mongodb.com/)

## Links

* [Api Documentation](http://ec2-13-58-109-241.us-east-2.compute.amazonaws.com:8080/swagger-ui.html)
* [/mutant](http://ec2-13-58-109-241.us-east-2.compute.amazonaws.com:8080/mutant) (ver documentación)
* [/stats](http://ec2-13-58-109-241.us-east-2.compute.amazonaws.com:8080/stats)
* [/info](http://ec2-13-58-109-241.us-east-2.compute.amazonaws.com:8080/actuator/info)